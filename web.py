#Modulos
from flask import Flask, render_template, flash, request
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
import MySQLdb

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
 
data = []

class ReusableForm(Form):
    name = TextField('Name:', validators=[validators.required()])
 
 
@app.route("/", methods=['GET', 'POST'])
def hello():
    form = ReusableForm(request.form)
 
    print(form.errors)

    if request.method == 'POST':
        name=request.form['name']
        password = request.form['password']
 
        ################MYSQL###########################
        db = MySQLdb.connect(host="172.22.200.144",
             user=name,
             passwd=password,
             db="") 

        cur = db.cursor()
        cur.execute("SHOW DATABASES")
        for row in cur.fetchall():
            data.append(row[0])
        ################################################

        if form.validate():
            # Save the comment here.
            flash(data)
        else:
            flash('Error: All the form fields are required. ')
            
    return render_template('hello.html', form=form)

if __name__ == "__main__":
    app.run()